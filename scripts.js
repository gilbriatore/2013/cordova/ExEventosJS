
function mostrarMensagem() {
    window.alert("Botão foi clicado!");   
}  

//O evento .onload só ocorre após o carregamento da página.
window.onload = function(){  
    var btnClique = document.getElementById("btn");
    btnClique.onclick = mostrarMensagem; 
} 